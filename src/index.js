import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.scss";
import Form from "./pages/Form";

ReactDOM.render(
  <React.StrictMode>
    <Form pageTitle="Informace o osobách a domácnostech" />
  </React.StrictMode>,
  document.getElementById("root")
);
