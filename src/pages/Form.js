import ContentWrapper from "../components/ContentWrapper";
import PersonForm from "../components/PersonForm";
import HouseholdCosts from "../components/HouseholdCosts";
import Comments from "../components/Comments";
import FormActions from "../components/FormActions";

const Form = props => {
  const { pageTitle } = props;
  return (
    <div className="container py-5">
      <h1 className="h5">{pageTitle}</h1>
      <ContentWrapper title="Domácnost 1">
        <div className="row">
          <div className="col-md-6 border-end">
            <PersonForm />
          </div>
          <div className="col-md-6">
            <HouseholdCosts />
          </div>
        </div>
        <Comments />
        <FormActions
          onStorno={event => console.log(event)}
          onSave={event => console.log(event)}
          onDocumentsClick={event => console.log(event)}
          onPrint={event => console.log(event)}
          onReCall={event => console.log(event)}
          onDone={event => console.log(event)}
        />
      </ContentWrapper>
    </div>
  );
};

export default Form;
