import React, { useState } from "react";
import { FaUserAlt, FaPlusCircle } from "react-icons/fa";

const personsData = {
  id: 1234456789,
  name: "Eva Kafurtová",
  type: "žadatel",
  salary: 37850
};

const PersonForm = () => {
  const [persons, setPersons] = useState([personsData]);
  console.log();
  return (
    <div className="person-form">
      <div className="row">
        {persons.map((person, i) => (
          <div className="col-md-4 mb-4" key={i}>
            <div
              className="person-card shadow-sm rounded text-center p-3"
              key={i}
            >
              <div className="display-1 mb-3">
                <FaUserAlt />
              </div>
              <strong>{person.name}</strong>
              <div>{person.id}</div>
              <strong className="d-block my-3">{person.type}</strong>
              <div>
                <div className="mb-1">Celkový příjem klienta</div>
                <strong className="person-card__salary h5">
                  {person.salary} Kč
                </strong>
              </div>
            </div>
          </div>
        ))}
        <div className="col-md-4 mb-4">
          <button
            className="person-form__add-btn shadow-sm btn h-100 w-100"
            onClick={() => setPersons([...persons, personsData])}
          >
            <div className="person-card__icon display-1 mb-3">
              <FaPlusCircle />
            </div>

            <div>Přidat osobu</div>
          </button>
        </div>
      </div>
      <div>
        <button className="btn btn-success text-white ">
          Přidat osobu z jiné domácnosti
        </button>
      </div>
    </div>
  );
};

export default PersonForm;
