import React from "react";

const Input = props => {
  const {
    label,
    placeholder,
    type,
    optionalLabel,
    onChange,
    required,
    value
  } = props;
  return (
    <div className="form-group">
      <label>{label}</label>
      <input
        type={type ?? "text"}
        className="form-control"
        placeholder={placeholder ?? ""}
        onChange={ev => onChange(ev.target.value)}
        value={value}
        required={required}
      />
      <small className="form-text text-muted">{optionalLabel}</small>
    </div>
  );
};

export default Input;
