import React from "react";

const ContentWrapper = props => {
  const { children, title } = props;
  return (
    <div className="content-wrapper my-5">
      <h1 className="content-wrapper__title">{title}</h1>
      <div className="content-wrapper__inner p-3">{children}</div>
    </div>
  );
};

export default ContentWrapper;
