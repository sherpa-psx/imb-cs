import React, { useState } from "react";
import { MdArrowDropDown, MdArrowDropUp } from "react-icons/md";
import dayjs from "dayjs";
import NewCommentForm from "./NewCommentForm";

dayjs.locale("cs");

const author = {
  id: 123456,
  name: "Jana Husáková"
};

const commentData = {
  title: "Tvorba hypoteční nabídky",
  date: dayjs(),
  author,
  content:
    "Systematicky zadostiučinění písmen má života, jedno § 61 i uložené umožňujícího, vyvolávajících fotografického občany 25 % těchto 23 o § 34, ať vcelku počítačové účetních v sdělovacího sdruženým prováděného. "
};

const Comments = () => {
  const [comments, setComments] = useState([commentData]);
  const [commentsVisible, setCommentsVisible] = useState(true);
  const [commentsFormVisible, setCommentsFormVisible] = useState(false);

  // Sort comments by the newest
  const sortedComments = comments.sort((a, b) => {
    return b.date - a.date;
  });

  const commentsVisibleHandler = () => {
    setCommentsVisible(!commentsVisible);
  };

  const addCommentHandler = data => {
    setComments([
      ...comments,
      {
        title: data.title,
        date: dayjs(),
        author,
        content: data.content
      }
    ]);
    setCommentsFormVisible(false);
  };

  const commentsFormVisibleHandler = () => {
    setCommentsFormVisible(!commentsFormVisible);
  };

  return (
    <div className="comments mt-5 mb-3 p-3 border">
      <header className="mb-3">
        <strong>
          Historie a komentáře{" "}
          <span className="text-danger">({comments.length})</span>
        </strong>
        <button
          className="btn btn-sm btn-link"
          onClick={() => commentsVisibleHandler()}
        >
          {commentsVisible ? "Skrýt" : "Zobrazit"}
          {commentsVisible ? <MdArrowDropUp /> : <MdArrowDropDown />}
        </button>
        <button
          className={`btn btn-sm text-white float-end ${
            commentsFormVisible ? "btn-danger" : "btn-success"
          }`}
          onClick={() => commentsFormVisibleHandler()}
        >
          {commentsFormVisible ? "Zrušit komentář " : "Přidat komentář"}
        </button>
      </header>
      {commentsFormVisible && (
        <NewCommentForm addComment={data => addCommentHandler(data)} />
      )}
      {commentsVisible && (
        <div className="comments__items-wrapper border-top border-end border-start">
          {sortedComments.map((comment, i) => (
            <div className="comment-item p-3 border-bottom" key={i}>
              <div className="fw-bold mb-2">{comment.title}</div>
              <div className="mb-2">
                <time className="d-inline-block me-5 ">
                  {comment.date.format("DD.MM.YYYY hh:mm")}
                </time>
                <span>
                  {comment.author.name} ({comment.author.id})
                </span>
              </div>
              <div>{comment.content}</div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Comments;
