import React, { useState } from "react";
import Input from "../Input";

const NewCommentForm = props => {
  const { addComment } = props;
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");

  const handleSubmit = ev => {
    ev.preventDefault();
    addComment({ title, content });
    setContent("");
    setTitle("");
  };

  return (
    <form onSubmit={ev => handleSubmit(ev)} className="mb-3">
      <Input
        placeholder="Zadejte nadpis komentáře"
        onChange={value => setTitle(value)}
        value={title}
        required
      />
      <div className="form-floating mt-2 mb-2">
        <textarea
          className="form-control"
          placeholder="Leave a comment here"
          id="floatingTextarea2"
          onChange={ev => setContent(ev.target.value)}
          value={content}
          required
          style={{ height: "200px" }}
        ></textarea>
        <label htmlFor="floatingTextarea2">Zadejte obsah komentáře</label>
      </div>
      <div className="d-flex justify-content-end">
        <input
          className="brn btn-sm btn-primary"
          type="submit"
          value="Odeslat komentář"
        />
      </div>
    </form>
  );
};

export default NewCommentForm;
