import React from "react";

const FormActions = props => {
  const {
    onStorno,
    onSave,
    onDocumentsClick,
    onPrint,
    onReCall,
    onDone
  } = props;
  return (
    <footer className="form-action d-flex justify-content-between">
      <div className="text-start">
        <button
          onClick={() => onStorno("Storno případu")}
          className="btn btn-danger"
        >
          Storno případu
        </button>
      </div>
      <div className="text-end">
        <button
          type="button"
          onClick={() => onSave("Uložit a zavřít")}
          className="btn btn-light ms-1"
        >
          Uložit a zavřít
        </button>
        <button
          type="button"
          onClick={() => onDocumentsClick("Zobrazit dokumenty")}
          className="btn btn-light ms-1"
        >
          Dokumenty
        </button>
        <button
          type="button"
          onClick={() => onPrint("Vytisknout modelaci")}
          className="btn btn-light ms-1"
        >
          Vytisknout modelaci
        </button>
        <button
          type="button"
          onClick={() => onReCall("Opakovat volání")}
          className="btn btn-light ms-1"
        >
          Opakovat volání
        </button>
        <button
          className="btn btn-primary ms-1"
          onClick={() => onDone("Sjednána schůzka")}
        >
          Sjednána schůzka
        </button>
      </div>
    </footer>
  );
};

export default FormActions;
