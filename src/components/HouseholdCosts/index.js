import React from "react";
import Input from "../Input";

const inputs = [
  {
    label: "Výdaje na domácnost",
    type: "number"
  },
  {
    label: "Počet členů domácnosti",
    type: "number"
  },
  {
    label: "Splátky úvěrů v FSČS",
    type: "number",
    optionalLabel: "Pomoc s výpočtem"
  },
  {
    label: "Splátky úvěrů v jiných bankách",
    type: "number"
  },
  {
    label: "Ostatní závazky (výživné)",
    type: "number"
  }
];

const HouseholdCosts = () => {
  return (
    <div className="household-costs">
      <div className="row">
        {inputs.map((input, i) => (
          <div className="col-md-6 mb-2" key={i}>
            <Input
              label={input.label}
              type={input.type}
              optionalLabel={input.optionalLabel}
              onChange={value => console.log(`${input.label}: ${value}`)}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default HouseholdCosts;
